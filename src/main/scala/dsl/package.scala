import scalaz.Free

package object dsl {
  type UserRepo[A] = Free[UserRepoF, A]
}
