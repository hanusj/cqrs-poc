package dsl

import domain.User

import scala.concurrent.Future
import scalaz.Free


sealed trait UserOperation
case class ChangeName(name: String) extends UserOperation
case class ChangeEmail(email: String) extends UserOperation
case class ChangePassword(pswd: String) extends UserOperation
case class UserCreated(name: String, email: String, pswd: String) extends UserOperation

sealed trait UserRepoF[A]
case class Query(id: String) extends UserRepoF[User]
case class Create(id: String, name: String, email: String, pswd: String) extends UserRepoF[Unit]
case class Command(id: String, operation: UserOperation) extends UserRepoF[Unit]

trait UserRepository {
  private implicit def liftEvent[Next](event: UserRepoF[Next]): UserRepo[Next] = Free.liftF(event)

  def query(id: String): UserRepo[User] = Query(id)

  protected def create(id: String, name: String, email: String, pswd: String): UserRepo[Unit] = Create(id, name, email, pswd)

  protected def command(id: String, operation: UserOperation): UserRepo[Unit] = Command(id, operation)
}

