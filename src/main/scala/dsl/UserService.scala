package dsl

import domain.User

import scala.concurrent.Future


trait UserService[User] {
  def createUser(id: String, name: String, email: String, pswd: String): UserRepo[User]
  def queryById(id: String): UserRepo[User]
  def changeName(id: String, name: String): UserRepo[User]
}

object UserService extends UserService[User] with UserRepository {

  override def changeName(id: String, name: String): UserRepo[User] = {
    for {
      _ <- command(id, ChangeName(name))
      u <- query(id)
    } yield u
  }

  override def createUser(id: String, name: String, email: String, pswd: String): UserRepo[User] = {
    for {
      _ <- command(id, UserCreated(name, email, pswd))
      u <- query(id)
    } yield u
  }

  override def queryById(id: String): UserRepo[User] = {
    query(id)
  }
}
