package interpreter

import dsl.UserRepo

import scalaz.concurrent.Task

trait UserRepoInterpreter {
  def apply[A](action: UserRepo[A]): Task[A]
}
