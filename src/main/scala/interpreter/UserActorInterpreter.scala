package interpreter

import java.util.concurrent.TimeUnit

import akka.NotUsed
import akka.actor.{Actor, ActorPath, ActorSystem, ExtendedActorSystem, Props}
import akka.persistence.query.{EventEnvelope, Offset, PersistenceQuery}
import akka.persistence.query.journal.leveldb.scaladsl.LeveldbReadJournal
import akka.persistence.{PersistentActor, SnapshotOffer}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.util.Timeout
import domain.User
import dsl._

import scalaz.concurrent.Task
import Task.{fail, now}
import scala.util.{Failure, Success}
import scalaz.~>

case class UserActorInterpreter() extends UserRepoInterpreter {

  val system = ActorSystem("user-actor-system")
  implicit val mat = ActorMaterializer()(system)
  val readJournal = PersistenceQuery(system).readJournalFor[LeveldbReadJournal](LeveldbReadJournal.Identifier)


  val step: UserRepoF ~> Task = new (UserRepoF ~> Task) {
    override def apply[A](fa: UserRepoF[A]): Task[A] = fa match {
      case q @ Query(id) => {
        val actor = system.actorSelection(s"target/example/journal/$id")
        import akka.pattern.ask
        implicit val askTimeout = Timeout(30, TimeUnit.SECONDS)

        (actor ? q).mapTo[User].value match {
          case Some(t) => t match {
            case Success(u) => now(u)
            case Failure(e) => fail(e)
          }
          case None => fail(new RuntimeException("vlaaa"))
        }
      }
      case Command(id, operation) =>
        println("Sending command")
        now(system.actorOf(Props[UserPersistentActor]) ! operation)

    }
  }

  override def apply[A](action: UserRepo[A]): Task[A] = action.foldMap(step)
}

class UserPersistentActor extends PersistentActor {

  override def persistenceId: String = "sample-id"

  var state = User(persistenceId, "", "", "")

  private def updateState(operation: UserOperation): Unit = operation match {
    case UserCreated(name, email, pswd) => User(persistenceId, name, email, pswd)
    case ChangeName(name) => state.copy(name = name)
    case ChangeEmail(email) => state.copy(email = email)
    case ChangePassword(pswd) => state.copy(pswd = pswd)
  }

  override def receiveRecover: Receive = {
    case operation: UserOperation => updateState(operation)
    case SnapshotOffer(_, snapshotState: User) => state = snapshotState
  }

  override def receiveCommand: Receive = {
    case q: Query => sender ! state
    case operation: UserOperation => persist(operation)(operation => {
      println(operation)
      updateState(operation)
      saveSnapshot(state)
    })
  }
}



