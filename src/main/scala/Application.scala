import interpreter.UserActorInterpreter

object Application extends App {

  import dsl.UserService._

  override def main(args: Array[String]): Unit = {
    val user = for {
      u <- createUser("1234", "Petr Vyskocil", "muj@mail.cz", "12345")
    } yield u

    val interpreter = UserActorInterpreter()

    interpreter.apply(user)
  }
}
