name := "free-sourcing"

version := "0.1"

scalaVersion := "2.12.5"

val scalazVersion = "7.3.0-M21"

val akkaVersion = "2.5.4"

libraryDependencies ++= Seq(
  "org.scalaz" %% "scalaz-core" % scalazVersion,
  "org.scalaz" %% "scalaz-concurrent" % scalazVersion,
  "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
  "com.typesafe.akka" %% "akka-persistence-query" % akkaVersion,
  "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8"
)